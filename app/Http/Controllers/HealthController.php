<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
class HealthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    public function isAlive(Request $request) 
    {   
        
        $result = new \stdClass;
        $result->status = 'ALIVE';
        
        // Connects to web_db
        try{ 
            $x = DB::table('dbo.dtproperties')->take(1)->get();
            $result->dbconnected = 1;
        }
        catch(\Exception $ex){
            $result->dbconnected = 0;
            \Log::error($ex);
        }
        
        return new JsonResponse(['Code' => 200, "HealthCheck" => $result]);
        
    }

}
